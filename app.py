from smiles_converter import *
from os import remove
import os
import pickle
from tkinter import * 
from tkinter import filedialog
from tkinter import ttk
import random
from datetime import datetime
from PIL import ImageTk, Image
import smiles_vizualizer
import ttkthemes

FILE_TYPES = (("Text file", "*.txt"),("All files", "*.*"))

class HistoryNode:
    def __init__(self, file_name, structure, prediction, picture_path, pred_class):
        self.file_name = file_name
        self.structure = structure
        self.prediction = prediction
        self.timestamp = datetime.now() 
        self.picture_path = picture_path
        self.prediction_class = pred_class 

class App:
    def __init__(self):
        self.is_new = True
        self.current_molecule_file = ""
        self.history_nodes = []
        self.read_history_nodes()
        self.current_evaluation = 3
        self.current_structure = ""
        self.current_picture_path = ""
        self.idx = 0
        self.current_prediction_class = "Low"

    def set_molecule_file(self, file_path):
        self.current_molecule_file = file_path
        self.is_new = True

    def save_molecule(self):
        self.history_nodes.append(HistoryNode(self.current_molecule_file, self.current_structure, self.current_evaluation, self.current_picture_path, self.current_prediction_class))
        self.write_history_nodes()

    def get_molecule_structure(self):
        if self.current_structure != "":
            return self.current_structure
        elif self.current_molecule_file != "":
            _file_name = os.path.basename(self.current_molecule_file)
            if _file_name.split(".")[-1] == "sdf":
               return smiles_from_molfile(self.current_molecule_file)
    
            with open(self.current_molecule_file, "r") as f:
                structure = f.read()
                self.current_structure = structure
                return structure
        return ""

    def evaluate(self):
        if self.current_molecule_file == "":
            self.current_molecule_file = "ungrouped"
        pred,  pred_class, path = smiles_vizualizer.evaluate(str(self.current_structure)) 
        self.current_evaluation = pred[0][0]
        self.current_picture_path = path 
        self.current_prediction_class = pred_class
        self.save_molecule()

    def write_history_nodes(self):
        if os.path.exists("./history.bio"):
            remove("history.bio")

        pickle.dump(self.history_nodes, open("./history.bio", "wb"))
    
    def read_history_nodes(self):
        try:
            self.history_nodes = pickle.load(open("./history.bio", "rb"))
        except:
            pass

    def get_history_nodes(self):
        return self.history_nodes

    def set_current(self, idx):
        self.is_new = False
        self.idx = idx
        node = self.history_nodes[len(self.history_nodes) - idx - 1]
        self.current_evaluation = node.prediction
        self.current_molecule_file = node.file_name
        self.current_picture_path = node.picture_path
        self.current_structure = node.structure 
        self.current_prediction_class = node.prediction_class

    def get_image_path(self):
        return self.current_picture_path
    
    def get_current_prediction(self):
        return self.current_evaluation

    def get_current_prediction_class(self):
        return self.current_prediction_class

def open_file():
    file_name = filedialog.askopenfilename(initialdir="/home/asusnik/lek-hack/", title="Select molecule file", filetypes=FILE_TYPES) 
    if not file_name:
        return
        
    backend.set_molecule_file(file_name)
    rebuild_structure_text()

def rebuild_structure_text():
    molecule_structure.delete('1.0', END)
    molecule_structure.insert(END, backend.get_molecule_structure())  

def save():
    backend.save_molecule()
    rebuild_history_list()    

def has_multiple_sturucts(structs):
    return len(structs.split("\n")) > 1
        
def evaluate_button_clicked():
    structure = molecule_structure.get("1.0", END)
    if has_multiple_sturucts(structure):
        structs = structure.split("\n") 
        for struct in structs:
            backend.current_structure = struct
            backend.evaluate()
            load_picture()
            rebuild_history_list()
    else:
        backend.current_structure = structure
        backend.evaluate()
        load_picture()
        rebuild_history_list()

def get_foreground(pred_class):
    if pred_class == "Low":
        return "red"
    if pred_class == "Medium":
        return "orange"
    if pred_class ==  "High":
        return "green"

def rebuild_history_list():
    history_list_view.delete(0, END)
    history_nodes = backend.get_history_nodes()
    for node in history_nodes:
        history_list_view.insert(0, os.path.basename(node.file_name) + "    " + str(node.prediction) if node.prediction else "/")
        history_list_view.itemconfig(0, foreground=get_foreground(node.prediction_class))

def open_file_e(event):
    open_file()

def switch_molecule(event):
    selection = history_list_view.curselection()
    backend.set_current(selection[0]) 
    rebuild_structure_text()
    load_picture()

def load_picture():
    image_path = backend.get_image_path()
    if image_path == "":
        return 
    img = ImageTk.PhotoImage(Image.open(image_path))
    eval_picture_canvas.create_image(20, 20, anchor=NW, image=img)
    eval_picture_canvas.image = img
    bio_avail_result.config(text=f"{backend.get_current_prediction_class()}", fg=get_foreground(backend.get_current_prediction_class()))
    aprox_pred_val.config(text=f"{round(backend.get_current_prediction() * 100, 2)} %")

def import_model():
    pass

backend = App()
app = Tk()
main_menu = Menu(app)
history_label = Label(app, text="History")
history_label.grid(column=0, row=0)

history_list_view = Listbox(app, width=30)
history_list_view.grid(column=0, row=1, rowspan=3, sticky=N+S)
history_list_view.bind("<Double-1>", switch_molecule)
rebuild_history_list()

molecule_structure_label = Label(app, text="Molecule definition")
molecule_structure_label.grid(column=1, row=0)

molecule_structure = Text(app)
molecule_structure.grid(column=1, row=1)
evaluate_button = Button(app, text="Evaluate", command=evaluate_button_clicked)
evaluate_button.grid(column=1, row=3)

molecule_pic = Label(app, text="Molecule structure")
molecule_pic.grid(column=2, row=0)

eval_picture_canvas = Canvas(app, width=500, height=500)
eval_picture_canvas.grid(column=2, row=1)
load_picture()

bio_avail_label = Label(app, text="Bioavailability")
bio_avail_label.grid(column=2, row=2)

bio_avail_result = Label(app, text="")
bio_avail_result.grid(column=2, row=3)

aprox_pred = Label(app, text="Aproximate prediction")
aprox_pred.grid(column=2, row=4)

aprox_pred_val = Label(app, text="")
aprox_pred_val.grid(column=2, row=5)

main_menu.add_command(label="Open file", command=open_file)
main_menu.add_command(label="Save molecule", command=save)
main_menu.add_command(label="Import model", command=import_model)

app.geometry("1366x600")
app.config(menu=main_menu)
app.bind("<Control-o>", open_file_e)
s = ttkthemes.themed_style.ThemedStyle(app)
s.theme_use("breeze")
try:
    app.mainloop()
except:
    exit(1)
