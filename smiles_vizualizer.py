import numpy as np
import pandas as pd
import uuid
import rdkit
from rdkit import Chem
from rdkit.Chem import Draw
from rdkit.Chem.Draw import rdMolDraw2D
from rdkit.Chem import rdDepictor
from rdkit.Chem.rdchem import Mol

import matplotlib.pyplot as plt
import tensorflow as tf
from smiles_converter import *
import shap

# load model
model = tf.keras.models.load_model(
    "models/chemixnet_cnn_lip/chemixnet_cnn_lip_26-9_12-51.h5"
)

# load data
# dictionary for data import
data_dict = {
    "path_to_smiles": "dataset_lek/smiles_lek.csv",
    "delimiter": ";",
    "title_line": False,
}
molecules = read_smiles_file(
    data_dict["path_to_smiles"],
    delimiter=data_dict["delimiter"],
    title_line=data_dict["title_line"],
)
smiles, y_labels = molecules_to_features(molecules)
fingers = molecules_to_fingerprint(molecules)
smiles = smiles.reshape(len(smiles), 400, 42)
lipinski = molecules_to_lipinski(molecules)

# SHAP EXPLAINER
explainer = shap.DeepExplainer(
    model, [smiles, fingers, lipinski]
)  # soon to add lipinski


def evaluate(smiles_string):
    # make mol object
    mol = Chem.MolFromSmiles(smiles_string)
    # preprocess data
    mol_smiles, _ = molecules_to_features([mol], property_name=None)
    mol_smiles = mol_smiles[0][0]
    mol_finger = molecules_to_fingerprint([mol])[0]
    mol_lipinski = molecules_to_lipinski([mol])[0]

    mol_smiles = mol_smiles.reshape(1, mol_smiles.shape[0], mol_smiles.shape[1])
    mol_finger = mol_finger.reshape(1, mol_finger.shape[0])
    mol_lipinski = mol_lipinski.reshape(1, mol_lipinski.shape[0])

    # make predictions
    prediction = model.predict(
        [mol_smiles, mol_finger, mol_lipinski]
    )  # soon to add mol_lipinski

    # make picture
    path_to_pic = viz_shap_values(mol, explainer, mol_smiles, mol_finger, mol_lipinski)
    pred_class = "Low"

    if prediction[0][0] > 0.33 and prediction[0][0] < 0.66:
        pred_class = "Medium"
    elif prediction[0][0] >= 0.66:
        pred_class = "High"
    return (prediction, pred_class, path_to_pic)

def create_atom_index(sm):
    index = np.zeros(len(sm)) - 1

    count = 0
    for i, char in enumerate(list(sm)):
        if char in ["C", "N", "O", "S", "c", "n", "o", "s"]:
            index[i] = count
            count += 1
    return index


def calc_shap_values(explainer, smiles_data, fingers_data, lipinski_data):
    return explainer.shap_values([smiles_data, fingers_data, lipinski_data])


def viz_shap_values(
    mol, explainer, smiles_data, fingers_data, lipinski_data, VIZ_TRESHOLD=0.3
):
    sm = Chem.MolToSmiles(mol, kekuleSmiles=True, isomericSmiles=True)
    shap_values = calc_shap_values(explainer, smiles_data, fingers_data, lipinski_data)[0][0][0]
    atom_index = create_atom_index(sm)
    atom_places = [(a >= 0) for a in atom_index] + [False] * (400 - len(sm))
    shap_atom = pd.DataFrame(shap_values[atom_places])
    shap_atom["sum"] = shap_atom.abs().sum(axis=1)

    border_up = (
        shap_atom["sum"].sort_values(ascending=False).quantile(q=1 - VIZ_TRESHOLD)
    )
    shap_atom["above"] = shap_atom.apply(lambda row: row["sum"] >= border_up, axis=1)
    border_low = shap_atom["sum"].sort_values(ascending=False).quantile(q=VIZ_TRESHOLD)
    shap_atom["below"] = shap_atom.apply(lambda row: row["sum"] <= border_low, axis=1)

    shap_atom["sum_sign"] = shap_atom.sum(axis=1)
    ids_high = shap_atom[shap_atom["above"] == True].index
    ids_low = shap_atom[shap_atom["below"] == True].index

    pos = [id_val for id_val in ids_high]
    neg = [id_val for id_val in ids_low]

    # check for bonds
    bonds_pos = [Mol.GetBondBetweenAtoms(mol, j, k) for j in pos for k in pos]
    bonds_pos = [k.GetIdx() for k in bonds_pos if k != None]

    bonds_neg = [Mol.GetBondBetweenAtoms(mol, j, k) for j in neg for k in neg]
    bonds_neg = [k.GetIdx() for k in bonds_neg if k != None]

    atoms_colors = {}
    bonds_colors = {}
    for atom in pos:
        atoms_colors[atom] = (0.5, 1, 0.5)  # red
    for bond in bonds_pos:
        bonds_colors[bond] = (0.5, 1.0, 0.5)  # red

    for atom in neg:
        atoms_colors[atom] = (1, 0.5, 0.5)  # red
    for bond in bonds_neg:
        bonds_colors[bond] = (1, 0.5, 0.5)  # blue

    atoms = list(pos) + list(neg)
    bonds = list(bonds_pos) + list(bonds_neg)

    # DRAW SMILES IN SVG
    drawer = rdMolDraw2D.MolDraw2DCairo(500, 500, 500, 500)
    drawer.DrawMolecule(
        mol,
        highlightAtoms=atoms,
        highlightAtomColors=atoms_colors,
        highlightBonds=bonds,
        highlightBondColors=bonds_colors,
    )
    drawer.FinishDrawing()
    file_name = f"./assets/{str(uuid.uuid1())}.png"
    drawer.WriteDrawingText(file_name)
    return file_name
