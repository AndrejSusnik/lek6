"""MAIN SCRIPT FOR ANN TRAINING"""
import numpy as np
import pandas as pd
import tensorflow as tf

# our own imports
from NN_Params import NNParams
from CNNWrapper import CNNWrapper
from model_scripts.example_model import (
    ExampleModel,
    CheMixNet_CNN,
    CheMixNet_CNN_lip,
    CheMixNet_CNN_mol,
)  # soon (*)
from model_scripts.cnn_mlp_model import cnnMlpModel
from smiles_converter import *
from random_grid_search import RandomGridSearch

np.random.seed(42)
tf.random.set_seed(42)

# add your custom model in this dictionary
model_type_dict = {
    "example_model": ExampleModel,
    "chemixnet_cnn": CheMixNet_CNN,
    "chemixnet_cnn_lip": CheMixNet_CNN_lip,
    "cnn_mlp_model": cnnMlpModel,
    "chemixnet_cnn_feat": CheMixNet_CNN_mol,
}


def train_models(
    model_type,
    N_ITERATIONS,
    data_dict,
    stable_dict={},
    grid_search_dict={},
    save_model=False,
    write_log=True,
    shady_quant=False,
    divide_100=False,
    into_10_classes=False,
    into_20_classes=False,
):
    """FROM HERE ON OUT EVERYTHING SHOULD BE AUTOMATED"""

    molecules = read_smiles_file(
        data_dict["path_to_smiles"],
        delimiter=data_dict["delimiter"],
        title_line=data_dict["title_line"],
    )

    # transform molecules into training dataset
    smiles, y_labels = molecules_to_features(
        molecules,
        shady_quant=shady_quant,
        divide_100=divide_100,
        into_10_classes=into_10_classes,
        into_20_classes=into_20_classes,
    )
    fingers = molecules_to_fingerprint(molecules)
    smiles = smiles.reshape(len(smiles), 400, 42)
    lipinski = molecules_to_lipinski(molecules)
    # features = calculate_features(molecules)

    if N_ITERATIONS <= 0:
        N_ITERATIONS = 1
        for key, val in grid_search_dict.items():
            N_ITERATIONS *= len(val)
    if N_ITERATIONS >= 10 ** 5:
        N_ITERATIONS = 10 ** 5

    nn_params = NNParams(model_type=model_type, dataset=data_dict["path_to_smiles"])
    rnd_grid = RandomGridSearch(nn_params, grid_search_dict, N_ITERATIONS)

    # set epochs and batch_size
    for name, param in stable_dict.items():
        setattr(nn_params.train_params, name, param)

    for count in range(N_ITERATIONS):

        # call random grid and get new nn_params dict
        new_nn_params = next(rnd_grid.new_nn_params)

        print(new_nn_params.model_params.cnn_1_filters)

        model_class = model_type_dict[model_type](new_nn_params)
        model_class.run_training(
            smiles=smiles,
            fingers=fingers,
            props=lipinski,  # add lipinski if you have a model that includes that
            mol_features=None,
            y_labels=y_labels,
            save_model=save_model,
            write_log=write_log,
        )


if __name__ == "__main__":
    """DEFINE EVERTHING YOU LIKE HERE"""
    # define this as a model you would like to train

    MODEL_TYPE = "chemixnet_cnn_lip"
    N_ITERATIONS = -1  # set -1 if you want all

    # dictionary for stable changes to the training parameters
    stable_train_params = {
        "NUM_EPOCHS": 100,
        "BATCH_SIZE": 32,
        "loss_func": "mse",
        "metrics": ["mae"],
        "verbose": 2,
        "test_size": 0.2,
        "val_size": 0.2,
    }

    # dictionary for data import
    data_dict = {
        "path_to_smiles": "dataset_lek/smiles_lek.csv",
        "delimiter": ";",
        "title_line": False,
    }

    # dictionary for grid search, changing model parameters
    grid_search_dict = {
        # CNN1
        "num_of_cnn_smiles": [3],
        "cnn_1_filters": [100],  # [10, 20],
        "cnn_1_kernel_size": [7],  # [3, 5, 7],
        "cnn_pool_size_1": [3],  # [2, 3, 5],
        "cnn_1_stride": [2],  # [1, 2],
        "cnn_1_drop": [0.0],
        # CNN2
        "cnn_2_filters": [100],
        "cnn_2_kernel_size": [5],
        "cnn_pool_size_2": [5],
        "cnn_2_stride": [2],
        # CNN3
        "cnn_3_filters": [100],
        "cnn_3_kernel_size": [5],
        "cnn_pool_size_3": [5],
        "cnn_3_stride": [1],
        # CNN DENSE
        "cnn_dense_units": [20],
        "cnn_dense_act": ["elu"],
        "cnn_dense_drop": [0.0],
        # FINGERS
        "f_dense_1_units": [200],
        "f_dense_1_act": ["elu"],
        "f_dense_2_units": [100],
        "f_dense_3_units": [1],
        # LIPINSKI
        "lip_dense_1_units": [5],
        "lip_dense_1_act": ["elu"],
        "lip_dense_2_units": [3],
        "lip_dense_2_act": ["elu"],
        # LAST DENSE
        "num_of_dense_last": [2],
        "l_dense_1_units": [20],
        "l_dense_1_act": ["elu"],
        "l_dense_1_drop": [0.0],
        "l_dense_2_units": [10],
        "l_dense_2_act": ["elu"],
        "l_dense_2_drop": [0.0],
        # OUTPUT DENSE
        "output_dense_units": [1],
        "output_dense_act": ["sigmoid"],
    }

    train_models(
        model_type=MODEL_TYPE,
        N_ITERATIONS=N_ITERATIONS,
        data_dict=data_dict,
        stable_dict=stable_train_params,
        grid_search_dict=grid_search_dict,
        save_model=True,
        write_log=True,
        shady_quant=False,
        divide_100=False,
        into_10_classes=False,
        into_20_classes=True,
    )
