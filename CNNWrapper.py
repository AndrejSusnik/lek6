import numpy as np
import pandas as pd
from datetime import datetime
import os

# import whatever layers you need
from tensorflow.keras.layers import Dense, Conv1D, Conv2D, Dropout, concatenate
from tensorflow.keras import Input, Model, optimizers
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler

from NN_Params import NNParams


class CNNWrapper:
    def __init__(self, nn_params):
        self.nn_params = nn_params

        # separate train and model parameters
        self.tp = self.nn_params.train_params
        self.mp = self.nn_params.model_params

        # Give your architecture a custom name
        self.prefix_name = "wrapper"
        self.model = None

    def run_building(self, get_model=False):
        # gives your model a unique name
        self.generate_name()
        # builds a model
        self.build_model()
        # compiles your model
        self.compile_model()

        if get_model:
            return self.get_model()

    def build_model(self):
        # OVERRIDE THIS FUNCTION
        # this is just a place holder - look into ./model_scripts for example_model.py

        smiles = Input(shape=self.tp.smiles_shape)
        finger = Input(shape=self.tp.finger_shape)

        # Concatenate layer
        concat = concatenate([smiles, finger], axis=1)

        # Last (output) dense layer
        dense_final = Dense(
            units=self.mp.final_dense_units, activation=self.mp.final_dense_act
        )(concat)

        # BUILDING A MODEL (functional API)
        self.model = Model(inputs=[smiles, finger], outputs=dense_final, name=self.name)

    def compile_model(self):
        self.optimizer_instance = optimizers.get(self.tp.optimizer)

        # adjust optimizer
        self.optimizer_instance.lr.assign(self.tp.learning_rate)
        if self.tp.optimizer == "sgd" and self.tp.nesterov == True:
            self.optimizer_instance.nesterov = True

        self.model.compile(
            optimizer=self.optimizer_instance,
            loss=self.tp.loss_func,
            metrics=[self.tp.metrics],
        )

    def get_model(self):
        return self.model

    def generate_name(self):
        now = datetime.now()
        self.time_suffix = f"{now.day}-{now.month}_{now.hour}-{now.minute}"
        self.name = f"{self.prefix_name}_{self.time_suffix}"

    def run_training(
        self,
        smiles=None,
        fingers=None,
        props=None,
        mol_features=None,
        y_labels=None,
        save_model=True,
        write_log=True,
    ):
        """
        Takes in all vectors: smiles, fingerprints and chem_properties
        """
        # Build a model, if not yet built
        if not self.model:
            self.run_building()

        # split train and test dataset
        X_smiles, X_smiles_test, y_train, y_test = train_test_split(
            smiles,
            y_labels,
            test_size=self.tp.test_size,
            random_state=self.tp.random_state,
            shuffle=self.tp.shuffle_train_test,
        )
        X_smiles_train, X_smiles_val, y_train, y_val = train_test_split(
            X_smiles,
            y_train,
            test_size=self.tp.val_size,
            random_state=self.tp.random_state,
            shuffle=self.tp.shuffle_train_test,
        )
        # fingers
        X_fingers, X_fingers_test, y_train, y_test = train_test_split(
            fingers,
            y_labels,
            test_size=self.tp.test_size,
            random_state=self.tp.random_state,
            shuffle=self.tp.shuffle_train_test,
        )
        X_fingers_train, X_fingers_val, y_train, y_val = train_test_split(
            X_fingers,
            y_train,
            test_size=self.tp.val_size,
            random_state=self.tp.random_state,
            shuffle=self.tp.shuffle_train_test,
        )
        # lipinski - props
        if type(props) == np.ndarray:
            X_lipinski, X_lipinski_test, y_train, y_test = train_test_split(
                props,
                y_labels,
                test_size=self.tp.test_size,
                random_state=self.tp.random_state,
                shuffle=self.tp.shuffle_train_test,
            )
            X_lipinski_train, X_lipinski_val, y_train, y_val = train_test_split(
                X_lipinski,
                y_train,
                test_size=self.tp.val_size,
                random_state=self.tp.random_state,
                shuffle=self.tp.shuffle_train_test,
            )

            if self.tp.standard_props:
                scaler = StandardScaler()
                X_lipinski_train = scaler.fit_transform(X_lipinski_train)
                X_lipinski_val = scaler.transform(X_lipinski_val)
                X_lipinski_test = scaler.transform(X_lipinski_test)
            else:
                scaler = MinMaxScaler()
                X_lipinski_train = scaler.fit_transform(X_lipinski_train)
                X_lipinski_val = scaler.transform(X_lipinski_val)
                X_lipinski_test = scaler.transform(X_lipinski_test)

        if type(mol_features) == np.ndarray:
            print(np.argwhere(np.isnan(mol_features)))
            X_feat, X_feat_test, y_train, y_test = train_test_split(
                mol_features,
                y_labels,
                test_size=self.tp.test_size,
                random_state=self.tp.random_state,
                shuffle=self.tp.shuffle_train_test,
            )
            X_feat_train, X_feat_val, y_train, y_val = train_test_split(
                X_feat,
                y_train,
                test_size=self.tp.val_size,
                random_state=self.tp.random_state,
                shuffle=self.tp.shuffle_train_test,
            )

            if self.tp.standard_props:
                scaler = StandardScaler()
                X_feat_train = scaler.fit_transform(X_feat_train)
                X_feat_val = scaler.transform(X_feat_val)
                X_feat_test = scaler.transform(X_feat_test)
            else:
                scaler = MinMaxScaler()
                X_feat_train = scaler.fit_transform(X_feat_train)
                X_feat_val = scaler.transform(X_feat_val)
                X_feat_test = scaler.transform(X_feat_test)

        callbacks = []
        if self.tp.early_stopping:
            callbacks += [
                EarlyStopping(patience=self.tp.patience, restore_best_weights=True)
            ]

        print(self.model.summary())

        X_train_data = [X_smiles_train, X_fingers_train]
        X_val_data = [X_smiles_val, X_fingers_val]
        X_test_data = [X_smiles_test, X_fingers_test]
        if type(props) == np.ndarray:
            print("Adding lipinski")
            X_train_data = [X_smiles_train, X_fingers_train, X_lipinski_train]
            X_val_data = [X_smiles_val, X_fingers_val, X_lipinski_val]
            X_test_data = [X_smiles_test, X_fingers_test, X_lipinski_test]
        if type(mol_features) == np.ndarray:
            print("Adding molecular descriptors")
            X_train_data.append(X_feat_train)
            X_val_data.append(X_feat_val)
            X_test_data.append(X_feat_test)

        print(len(X_train_data))

        self.history = self.model.fit(
            X_train_data,
            y_train,
            validation_data=(X_val_data, y_val),
            epochs=self.tp.NUM_EPOCHS,
            batch_size=self.tp.BATCH_SIZE,
            callbacks=callbacks,
            verbose=self.tp.verbose,
        )

        # save model
        if save_model:
            self.save_model()

        self.eval = self.model.evaluate(X_test_data, y_test)

        self.score_loss = self.eval[0]
        self.score_metrics = list(self.eval[1:])

        # write into a log file and add model into model table
        if write_log:
            self.write_log_file()
            self.write_pandas_predictions(y_test, self.model.predict(X_test_data))

        # get this if you want to do any other analysis or vizualization
        self.X_test = X_test_data
        self.y_test = y_test

        if self.tp.save_test_set:
            if "test_sets" not in os.listdir("datasets"):
                os.mkdir("datasets/test_sets")
            os.mkdir(f"datasets/test_sets/{self.name}")
            os.mkdir(f"datasets/test_sets/{self.name}/smiles")
            for i in range(len(self.X_test[0])):
                np.savetxt(
                    f"datasets/test_sets/{self.name}/smiles/X_smiles_{i}.tsv",
                    self.X_test[0][i],
                    delimiter="\t",
                )
            np.savetxt(
                f"datasets/test_sets/{self.name}/fingers.tsv",
                self.X_test[1],
                delimiter="\t",
            )
            np.savetxt(
                f"datasets/test_sets/{self.name}/labels.tsv",
                self.y_test,
                delimiter="\t",
            )

        print(f"Evaluation score loss: {round(self.score_loss, 4)}")
        for i, score in enumerate(list(self.score_metrics)):
            print(f"Eval {list(self.tp.metrics)[i]}: {round(score, 4)}")
        print()

    def save_model(self):
        model_folders = os.listdir("models")
        if self.prefix_name not in model_folders:
            os.mkdir(f"models/{self.prefix_name}")

        self.model.save(f"models/{self.prefix_name}/{self.name}.h5")

    def write_log_file(self):

        params_dict = {}

        # get attributes from NN_params (model_type and dataset)
        nn_attr = [
            attr
            for attr in dir(self.nn_params)
            if not callable(getattr(self.nn_params, attr))
            and not attr.startswith("__")
            and attr not in ["model_params", "train_params"]
        ]
        for attr in nn_attr:
            params_dict[attr] = [getattr(self.nn_params, attr)]

        # get attibutes from TrainParams
        train_attr = [
            attr
            for attr in dir(self.nn_params.train_params)
            if not callable(getattr(self.nn_params.train_params, attr))
            and not attr.startswith("__")
        ]
        for attr in train_attr:
            params_dict[attr] = [getattr(self.nn_params.train_params, attr)]

        # get attributes from ModelParams
        model_attr = [
            attr
            for attr in dir(self.nn_params.model_params)
            if not callable(getattr(self.nn_params.model_params, attr))
            and not attr.startswith("__")
        ]
        for attr in model_attr:
            params_dict[attr] = [getattr(self.nn_params.model_params, attr)]

        # add scores and additional info inside
        params_dict["score_loss"] = [self.score_loss]
        params_dict["score_metrics"] = [self.score_metrics]

        params_panda = pd.DataFrame(params_dict, index=[self.name])
        params_panda.index.name = "model_ID"

        # check if any tables already exist
        tables = os.listdir("models/log_files")

        if f"{self.prefix_name}_table.tsv" not in tables:
            params_panda.reset_index().to_csv(
                f"models/log_files/{self.prefix_name}_table.tsv", sep="\t", index=False
            )

        else:
            model_table = pd.read_csv(
                f"models/log_files/{self.prefix_name}_table.tsv",
                sep="\t",
                index_col=["model_ID"],
            )

            new_table = pd.concat([model_table, params_panda], axis=0, join="outer")

            new_table.reset_index().to_csv(
                f"models/log_files/{self.prefix_name}_table.tsv", sep="\t", index=False
            )

        # write into a log file - TO DO

    def write_pandas_predictions(self, y_true, y_predictions):
        data = pd.DataFrame(
            {"True_values": y_true.flatten(), "Predictions": y_predictions.flatten()}
        )
        data.to_csv(
            f"datasets/test_sets/{self.name}_predictions.tsv", sep="\t", index=False
        )
