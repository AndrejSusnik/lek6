"""Function for randomly setting hyperparameters"""
import numpy as np
import time
import copy


class RandomGridSearch:
    def __init__(self, nn_params, grid_search_dict, N_ITERATIONS):

        self.nn_params = nn_params
        self.gs_dict = grid_search_dict
        self.N_ITER = N_ITERATIONS
        self.grid_params = sorted(self.gs_dict.keys())

        self.count_combinations()
        if self.combinations < self.N_ITER:
            raise ValueError("The combination level is too damn high!")

        self.generate_tuples()

        self.new_nn_params = iter(
            self.complete_new_params(tup) for tup in self.list_of_tuples
        )

    def count_combinations(self):
        comb = 1
        for key, values in self.gs_dict.items():
            comb *= len(values)
        self.combinations = comb

    def complete_new_params(self, tuple_of_params):

        new_params = copy.copy(self.nn_params)

        for i, new_value in enumerate(tuple_of_params):
            setattr(new_params.model_params, self.grid_params[i], new_value)

        return new_params

    def generate_tuples(self):

        list_of_tuples = []

        while len(set(list_of_tuples)) < self.N_ITER:
            new_tuple = []
            for key in self.grid_params:
                val = np.random.choice(np.array(self.gs_dict[key]), 1)[0]
                if type(val) == np.int64 or type(val) == np.int32:
                    val = int(val)
                new_tuple += [val]
            list_of_tuples += [tuple(new_tuple)]

        self.list_of_tuples = list(set(list_of_tuples))


# define RandomGridSearch
# to get new nn_params -> call next(RandomGridSearch.new_nn_params)
